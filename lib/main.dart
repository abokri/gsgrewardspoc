import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:gsg_rewards/routes/router.dart';
import 'package:injectable/injectable.dart';
import 'package:gsg_rewards/main.config.dart';
import 'home/home_page.dart';

final getIt = GetIt.instance;

@InjectableInit(
  initializerName: 'init', // default
  preferRelativeImports: true, // default
  asExtension: true, // default
)
void configureDependencies() => getIt.init();

void main() {
  configureDependencies(); // Initialize dependency injection
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final _appRouter = AppRouter();

  MyApp({super.key});

  @override
  Widget build(BuildContext context) {

    return MaterialApp.router(
      routerConfig: _appRouter.config(),
    );


  }
}