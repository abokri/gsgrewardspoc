import 'package:auto_route/auto_route.dart';
import 'package:offers/injectable.module.dart';
import 'package:offers/offers.dart';
import 'package:gsg_rewards/home/home_page.dart';
import 'package:offers/offers_module.dart';
part 'router.gr.dart';

@AutoRouterConfig(replaceInRouteName: 'Page,Route', modules: [OffersModule])
class AppRouter extends _$AppRouter {

  @override
  List<AutoRoute> get routes => [
    AutoRoute(page: HomeRoute.page, initial: true),
    AutoRoute(page: OffersRoute.page),
    AutoRoute(page: OffersSubRoute.page),
  ];
}
