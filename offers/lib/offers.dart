library offers;

import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:offers/offers_module.dart';

@RoutePage()
class OffersPage extends StatelessWidget {
  const OffersPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: InkWell(child: Text('Offers Tab Content'), onTap: (){
          context.router.push(OffersSubRoute());
      }),
    );
  }
}

@RoutePage()
class OffersSubPage extends StatelessWidget {
  const OffersSubPage({super.key});

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Text('Offers Sub Tab Content'),
    );
  }
}