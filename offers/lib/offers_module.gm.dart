// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouterGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

part of 'offers_module.dart';

abstract class _$OffersModule extends AutoRouterModule {
  @override
  final Map<String, PageFactory> pagesMap = {
    OffersRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const OffersPage(),
      );
    },
    OffersSubRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const OffersSubPage(),
      );
    },
  };
}

/// generated route for
/// [OffersPage]
class OffersRoute extends PageRouteInfo<void> {
  const OffersRoute({List<PageRouteInfo>? children})
      : super(
          OffersRoute.name,
          initialChildren: children,
        );

  static const String name = 'OffersRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [OffersSubPage]
class OffersSubRoute extends PageRouteInfo<void> {
  const OffersSubRoute({List<PageRouteInfo>? children})
      : super(
          OffersSubRoute.name,
          initialChildren: children,
        );

  static const String name = 'OffersSubRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}
